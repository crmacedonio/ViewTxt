# ViewTxt
This project, a very small project, is very useful for testing and learning [**Lazarus IDE**](https://www.lazarus-ide.org/) and your tools. The original version of this application (I called BrwScr) was developed in *Borland Delphi 3*, just used to navigate between source files and other types of files in plain text. The application has no intention of being more than it is, just that. 

Currently it can be compiled unchanged in: 
  - **Linux** , Debian and Ubuntu (32 and 64 bits)
  - **Windows**
  - **MacOS** (_no tested yet_)
